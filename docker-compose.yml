# docker-compose file for running paperless from the Docker Hub.
# This file contains everything paperless needs to run.
# Paperless supports amd64, arm and arm64 hardware.
#
# All compose files of paperless configure paperless in the following way:
#
# - Paperless is (re)started on system boot, if it was running before shutdown.
# - Docker volumes for storing data are managed by Docker.
# - Folders for importing and exporting files are created in the same directory
#   as this file and mounted to the correct folders inside the container.
# - Paperless listens on port 8000.
#
# In addition to that, this docker-compose file adds the following optional
# configurations:
#
# - Instead of SQLite (default), MariaDB is used as the database server.
#
# To install and update paperless with this file, do the following:
#
# - Copy this file as 'docker-compose.yml' and the files 'docker-compose.env'
#   and '.env' into a folder.
# - Run 'docker-compose pull'.
# - Run 'docker-compose run --rm webserver createsuperuser' to create a user.
# - Run 'docker-compose up -d'.
#
# For more extensive installation and update instructions, refer to the
# documentation.

version: "3.4"
services:
  broker:
    image: docker.io/library/redis:7
    restart: unless-stopped
    volumes:
      - ./redisdata:/data

  db:
    image: docker.io/library/mariadb:10
    restart: unless-stopped
    volumes:
      - ./dbdata:/var/lib/mysql
    environment:
      MARIADB_HOST: paperless
      MARIADB_DATABASE: paperless
      MARIADB_USER: paperless
      MARIADB_PASSWORD: paperless
      MARIADB_ROOT_PASSWORD: paperless
  
  tika:
    image: apache/tika:latest
    restart: unless-stopped
    ports:
      - "9998:9998"
  
  webserver:
    image: ghcr.io/paperless-ngx/paperless-ngx:latest
    restart: unless-stopped
    depends_on:
      - db
      - broker
      - tika
      - gotenberg
    ports:
      - "8834:8000"
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:8000"]
      interval: 30s
      timeout: 10s
      retries: 5
    volumes:
      - ./data:/usr/src/paperless/data
      - ./media:/usr/src/paperless/media
      - ./export:/usr/src/paperless/export
      - ./consume:/usr/src/paperless/consume
    env_file: docker-compose.env
    environment:
      LANG: de-DE.UTF-8
      PAPERLESS_REDIS: redis://broker:6379
      PAPERLESS_DBENGINE: mariadb
      PAPERLESS_DBHOST: db
      PAPERLESS_DBUSER: paperless # only needed if non-default username
      PAPERLESS_DBPASS: paperless # only needed if non-default password
      PAPERLESS_DBPORT: 3306
      PAPERLESS_TIKA_ENABLED: 1
      PAPERLESS_TIKA_ENDPOINT: http://tika:9998
      PAPERLESS_TIME_ZONE: Europe/Berlin
      PAPERLESS_OCR_LANGUAGES: deu eng
      PAPERLESS_OCR_LANGUAGE: deu
      PAPERLESS_CONSUMER_ENABLE_ASN_BARCODE: 1
      PAPERLESS_CONSUMER_ASN_BARCODE_PREFIX: "ASN"
      PAPERLESS_TIKA_GOTENBERG_ENDPOINT: http://gotenberg:3000
      PAPERLESS_FILENAME_FORMAT: "{created_year}/{correspondent}/{title}"
      PAPERLESS_TASK_WORKERS: 3
      PAPERLESS_THREADS_PER_WORKER: 1
      PAPERLESS_WORKER_TIMEOUT: 480
      PAPERLESS_CONSUMER_DELETE_DUPLICATES: 1
      PAPERLESS_CONSUMER_SUBDIRS_AS_TAGS: 1
      PAPERLESS_CONSUMER_RECURSIVE: 1
      #PAPERLESS_EMAIL_HOST: ""
      #PAPERLESS_EMAIL_PORT: 465
      #PAPERLESS_EMAIL_HOST_USER: ""
      #PAPERLESS_EMAIL_HOST_PASSWORD: ""
      #PAPERLESS_EMAIL_USE_TLS: 1
      PAPERLESS_OCR_IMAGE_DPI: 600

  gotenberg:
    image: docker.io/gotenberg/gotenberg:7.8
    restart: unless-stopped
    environment:
      CHROMIUM_DISABLE_ROUTES: 1
    # The gotenberg chromium route is used to convert .eml files. We do not
    # want to allow external content like tracking pixels or even javascript.
    # command:
      # - "gotenberg"
      # - "--chromium-disable-javascript=true"
      # - "--chromium-allow-list=file:///tmp/.*"
