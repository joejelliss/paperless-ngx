# First Steps

Install Docker Compose and docker

## Arch Linux Command

```
sudo pacman -S docker docker-compose
```

## Debian Linux

```
sudo apt install docker docker-compose
```